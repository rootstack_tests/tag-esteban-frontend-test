$(document).ready(function() {
  $.ajax({
    url: 'https://randomuser.me/api/?page=3&results=10',
    dataType: 'json',
    beforeSend: function(){
      $('#image-loader').show();
    },
    complete: function(){
      $('#image-loader').hide();
    },
    success: function (data) {
      if (data['results']) {
        var results = $("#profile").tmpl(data['results']);
        $.each(results, function (index, element) {
          var $newProfile = $(element);
          $newProfile.find('.profile-toogle-extra-info').on('click', function () {
            var profileId = $(this).data('profile-id');
            var $extraInfo = $('#' + profileId);

            if (!$extraInfo.is(':visible')) {
              $('.profile__details-extra').hide('slow');
              $extraInfo.removeClass('hide');
              $('.profile-toogle-extra-info').removeClass('rotate-icon-up');
              $(this).addClass('rotate-icon-up');
              $extraInfo.show('slow');
            }
            else {
              $extraInfo.addClass('hide');
              $(this).removeClass('rotate-icon-up');
              $extraInfo.hide('slow');
            }
          });
        })
        results.appendTo(".profile-list");
      }
    }
  });
});

